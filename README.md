# Usage

```
$ docker-compose run web mix phoenix.new . --app my_app
```

# Production

```
$ docker-compose run web mix deps.get --only prod
$ docker-compose run web brunch build --production
$ docker-compose run web MIX_ENV=prod mix phoenix.digest
```

```
environment:
  # - MIX_ENV=dev
  - MIX_ENV=prod
  - PORT=4000
```      