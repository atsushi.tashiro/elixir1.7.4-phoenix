FROM elixir:1.7.4

ENV NODE_VERSION 10.x
# ENV MIX_ENV=dev
ENV MIX_ENV=prod

RUN apt update \
    # && apt -y install locales && \
    # && locale-gen ja_JP.UTF-8 && \
    && rm -f /etc/localtime \
    && ln -fs /usr/share/zoneinfo/Asia/Tokyo /etc/localtime

# ENV LANG ja_JP.UTF-8
# ENV LC_CTYPE ja_JP.UTF-8
# RUN localedef -f UTF-8 -i ja_JP ja_JP.utf8


RUN curl -sL https://deb.nodesource.com/setup_${NODE_VERSION} | bash \
  # && apt update \
  && apt -y install nodejs \
  && apt -y install inotify-tools \
  && apt-get clean

RUN mix local.hex --force && \
    mix local.rebar --force && \
    mix archive.install --force https://github.com/phoenixframework/archives/raw/master/phoenix_new-1.2.4.ez --force

CMD mix phoenix.server

WORKDIR /app
